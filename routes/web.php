<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('users.index');
})->name('home');

Route::get('/create','UserController@viewCreate')->name('user-view-create');
Route::get('/update/{id}','UserController@viewUpdate')->name('user-view-update');

Route::post('/s/createUser', 'UserController@serviceCreateUser')->name('user-service-create');
Route::get('/s/userList', 'UserController@serviceListUser')->name('user-service-list');
Route::post('/s/deleteUser', 'UserController@serviceDeleteUser')->name('user-service-delete');
