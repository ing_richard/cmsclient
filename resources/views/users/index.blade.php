@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1 class="text-center">Lista de usuarios</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12 d-block mx-auto">
            <table id="table-user" class="table table-striped table-bordered dataTable" role="grid" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Correo</th>
                        <th>Fecha de creación</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="row mt-2 d-flex justify-content-center">
        <div class="col-md-6 col-sm-12 d-flex justify-content-center">
            <a href="{{ route('user-view-create') }}" class="btn btn-primary"> Crear usuarios</a>
        </div>
    </div>
@endsection

@section('scripts-footer')
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        var userListUrl = "{{ route('user-service-list') }}";
        var urlUserDelete = "{{ route('user-service-delete') }}";
        var urlHome = "{{ route('home') }}";
    </script>
@endsection