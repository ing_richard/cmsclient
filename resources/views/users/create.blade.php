@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12 mb-2">
            <h1 class="text-center">
                {{ ($isUpdate)?  'Actualización de datos' : 'Registro de usuarios' }} 
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12 mx-auto">
            <form id="form-client"  name="form-client" method="POST" autocomplete="off">
                
                @csrf

                <input type="hidden" name="isUpdate" value="{{ ($isUpdate)? true: false }}">
                @if ($isUpdate)
                    <input type="hidden" name="idUserUpdate" value="{{ ($id)? $id: 0 }}">
                @endif

                <div class="form-group">
                    <label for="nombre">Nombre *</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="{{ (isset($nombre))? $nombre:'' }}" required>
                    <div class="invalid-feedback">
                        Por favor ingrese su nombre
                    </div>
                </div>

                <div class="form-group">
                    <label for="apellido">Apellido *</label>
                    <input type="text" class="form-control" id="apellido" name="apellido" value="{{ (isset($apellido))? $apellido:'' }}" placeholder="">
                    <div class="invalid-feedback">
                        Por favor ingrese su apellidos
                    </div>
                </div>
                  
                <div class="form-group">
                    <label for="correo">Email *</label>
                    <input type="email" class="form-control" id="correo" name="correo" value="{{ (isset($correo))? $correo:'' }}" placeholder="">
                    <div class="invalid-feedback">
                        Por favor ingresa correo electrónico
                    </div>
                </div>

                <div class="form-group">
                    <label for="telefono">Teléfono</label>
                    <input type="text" class="form-control" id="telefono" name="telefono" value="{{ (isset($telefono))? $telefono:'' }}"placeholder="" >
                </div>

                <div class="form-group">
                    <label for="comentario">Comentario</label>
                    <textarea class="form-control" id="comentario" name="comentario" rows="3" >{{ (isset($comentario))? $comentario:'' }}</textarea>
                </div>

                <div class="form-group">
                    <button type="submit" id="btn-submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts-footer')
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        var userCrateUrl = "{{ route('user-service-create') }}";
        var urlHome = "{{ route('home') }}";
    </script>
@endsection