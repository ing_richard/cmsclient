var $ = require('jquery');
require('./lib/jquery.validate');
require('popper.js');
require('bootstrap');
require('datatables.net-bs4');
require('datatables.net-responsive');
var Inputmask = require('inputmask');

const Swal = require('sweetalert2');
const { data } = require('jquery');

const axios = require('axios').default;

let 
    $form = $('#form-client'),
    $btnSubmit = $("#btn-submit",$form),
    $tableUser = $("#table-user")
; 

function cleanForm($form) {
    
    let 
        $nombre = $('#nombre', $form),
        $apellido = $('#apellido', $form),
        $correo = $("#correo", $form),
        $telefono = $("#telefono", $form),
        $comentario = $("#comentario", $form)
    ;

    $nombre.val("");
    $apellido.val("");
    $correo.val("");
    $telefono.val("");
    $comentario.val("");

}

$(document).ready(function(){
    
    let urlUserList = (window.userListUrl !== "");
    let urlCreateUser = window.userCrateUrl;
    let urlHome = window.urlHome;
    
    // input mask for telefono field
    var selector = document.getElementById("telefono");
    if(selector) {
        var im = new Inputmask("9999999999");
        im.mask(selector);
    }
    
    // Setting Datatable users
    if(urlUserList){
       var tabla =  $('#table-user').DataTable( {
            processing: true,
            serverSide: true,
            buttons: [
                'create'
            ],
            responsive: true,
            "lengthMenu": [5, 10, 25, 50, 75, 100 ],
            "ajax": window.userListUrl,
            columns: [
                {data: 'id', name: 'id'},
                {data: 'nombre', name: 'nombre'},
                {data: 'apellido', name: 'apellido'},
                {data: 'email', name: 'email'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    }

    //Call delete service
    $('#table-user tbody').on('click','a.btn-delete',function(e){
        e.preventDefault();
        var data_form = tabla.row($(this).parents("tr")).data();
        let urlUserDelete = window.urlUserDelete;
        let formData = new FormData();
        formData.append('id', data_form.id);

        console.log(urlUserDelete);

        Swal.fire({
            title: '¿Estas seguro?',
            text: "No podrás revertir este cambio!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, Eliminar!'
          }).then((result) => {
            if (result.value) {
                axios.post
                (
                    urlUserDelete,
                    formData
                )
                .then(function (response) {
                    //console.log(response);
                
                    if(response.status == 200){
                        Swal.fire(
                            'Eliminado!',
                            'Este registro fué eliminado.',
                            'success'
                        )
                        
                        setTimeout( function () {
                            tabla.ajax.reload();
                        }, 2000 );
                    } 
                })
                .catch(function (error) {
                    console.log(error);
                });
            }
          })
    });
    
    $btnSubmit.click(function(e){
        
        //validate form
        $("form[name='form-client']").validate({
            rules: {
                nombre: "required",
                apellido: "required",
                correo: {
                  required: true,
                  email: true
                }
            },
            submitHandler: function($form) {
                // call service
                var formData = new FormData($form);

                axios.post
                (
                    urlCreateUser,
                    formData 
                )
                .then(function (response) {
                    //console.log(response);
                
                    cleanForm($('#form-client'));

                    if(response.status == 200){
                        if(response.isUpdate){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Usuario creado con exito',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }else {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Usuario actualizado con exito',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }

                        //Redirect to home
                        setTimeout(() => {
                            window.location.href=urlHome;
                        }, 3000);
                        
                    }
                    
                })
                .catch(function (error) {
                    console.log(error);
                });
            }
        });

    });

});
