<?php

namespace App\Http\Controllers;

use Validator;
use Yajra\Datatables\Datatables;
use DB;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{

    function viewCreate() {
        return view('users.create',["isUpdate" => false]);
    }

    function viewUpdate($id) {
        
        $user = User::find($id);
        
        if(!$user) {
            abort(403, 'Acción no autorizada.');
        }

        return view('users.create',[
            "id" => $user->id,
            "nombre" => $user->nombre,
            "apellido" => $user->apellido,
            "correo" => $user->email,
            "telefono" => $user->telefono,
            "comentario" => $user->description,
            "isUpdate" => true
        ]);
    }

    function serviceCreateUser(Request $request) {
        
        $result = [
            "status" => 400,
            "error" => "BadRequest"
        ];

        // if ( !$request->ajax() ) {
        //     error_log(__METHOD__.'error en la llamada del ajax');
        //     return response()->json($result, $result["status"]);
        // }

        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'apellido' => 'required',
            'correo' => 'required',
        ]);

        if ($validator->fails()) {
            //error_log(__METHOD__.'error en la validacion de los inputs');
            return response()->json([
                'msg'=> 'fieldsEmpty'
            ],500);
        }
        
        error_log(__METHOD__.' datos del usuario => '. var_export(User::find($request->idUserUpdate),true));
        // save and update user
        if( 
            isset($request->isUpdate) && 
            $request->isUpdate == 1
        ){
            
            if(isset($request->idUserUpdate)){
                $userObj = User::find($request->idUserUpdate);
                error_log(__METHOD__.' datos del usuario => '.var_export($userObj,true));
            }
            
        } else {
            $userObj = new User();
        }

        
        $userObj->nombre = (isset($request->nombre))? $request->nombre : '';
        $userObj->apellido = (isset($request->apellido))? $request->apellido : '';
        $userObj->email = (isset($request->correo))? $request->correo : '';
        $userObj->telefono = (isset($request->telefono))? $request->telefono : '';
        $userObj->description = (isset($request->comentario))? $request->comentario : '';

        try {
            $userObj->save();
        }
        catch ( \Exception $e )
        {
            do {
                error_log(__METHOD__ . ' 1.6 Exception code = ' . $e->getCode());
                error_log(__METHOD__ . ' 1.6 Exception msg  = ' . $e->getMessage());

                $e = $e->getPrevious();

            } while( !is_null($e) );

            $result["error"] = "InternalServerError";
            return response()->json($result, $result["status"]);                        
        }

        return response()->json( ["status"=>200,"isUpdate" => ($request->idUserUpdate)?true:false], 200 );
        
    }

    function serviceListUser(Request $request) {

        $result = [
            'status' => 400,
            'error' => 'BadRequest',
        ];        

        if ( !$request->ajax() ) {
            return response()->json($result, $result["status"]);
        }

        $user = User::get();

        $dataTableUser =  Datatables::of($user)
        ->addColumn('action', function ($user) {
            return "<a href=".route('user-view-update',$user->id)." class='btn btn-sm btn-primary'><i class='glyphicon glyphicon-edit'></i> Edit</a>".
            "<a href='' class='btn btn-sm btn-primary ml-2 btn-delete'>Delete</a>";
        })
        ->editColumn('id', '{{$id}}')
        ->removeColumn('password')
        ->toJson()
        ->getOriginalContent();

        $result = [
            'status' => 200,
            'data' => $dataTableUser
        ];

        return response()->json($dataTableUser, $result["status"]);
    }

    function serviceDeleteUser(Request $request) {
        
        $result = [
            "status" => 400,
            "error" => "BadRequest"
        ];
        
        $user = User::find($request->id);

        if(!$user) {

            $result["error"] = "User not found";
            return response()->json($result, $result["status"]);  
        }

        try {
            $user->delete();
        }
        catch ( \Exception $e )
        {
            do {
                error_log(__METHOD__ . ' 1.6 Exception code = ' . $e->getCode());
                error_log(__METHOD__ . ' 1.6 Exception msg  = ' . $e->getMessage());

                $e = $e->getPrevious();

            } while( !is_null($e) );

            $result["error"] = "InternalServerError";
            return response()->json($result, $result["status"]);                        
        } 

        return response()->json( ["status"=>200], 200 );
    }
}
