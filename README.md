## Instalar las dependencias con composer

Primero debes instalar todas las dependencias y paquetes con composer con el siguiente comando

-  composer install

Asegurate de estar en la raiz del proyecto desde la terminal.

## Instalar las dependencias con npm

En este paso necesitaras instalar las dependecias de los paquetes de nodejs con el manejador npm
con el siguiente comando.

- npm install

## Variables de entorno

Ubícate en la raiz del directorio del proyecto y busca un archivo que se llama **[.env.example] duplicalo y renombralo por **[.env]

## Verifica el archivo que contiene las variables de entorno

El archivo que tienes que revisar es el .env, el archivo que duplicaste y renombraste en el paso anterior,
alli veras las variables para la configuración de la base de datos, solo es necesario modificar, el nombre
de la base de datos, el usuario y el password si lo requiere

## Migrar la base de datos

Para migrar la base de datos debes de ejecutar el seguiente comando:

- php artisan migrate

si necesitas resetear la base de datos, hay un comando que te reestructura la base de datos y te la formatea,
es decir que te reconstruye las tablas y se eliminan los datos. para eso debes de ejecutar el siguiente comando.

- php artisan migrate:fresh

## Compilar los los modulos de nodejs

En este paso necesitamos compilar el npm para que corra toda la configuración que se hizo con webpack

- npm run dev

## Pasos Extras 

Por ultimos ejecuta el siguiente comando

- composer dump-autoload

## Correr servidor

Para correr el servidor necesitas ejecutar el siguiente comando

- php artisan serve

verifica en la terminal el host en la cual está corriendo, normalmente corre en el puerto 8000
